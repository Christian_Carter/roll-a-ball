﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

public class PlayerController : MonoBehaviour
{
    public float speed = 0;
    public TextMeshProUGUI countText;
    public GameObject winTextObject;
    public CountdownTimer timer;
    //explained tracker purpose below
    public int tracker = 0;

    private Rigidbody rb;
    public int count;
    //the count int represented both the points and how many pickup items there were in order to add more items with different points,
    //I had to create a different int to hold the points value int score.
    public int score;
    private float movementX;
    private float movementY;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        score = 0;

        SetCountText();
        winTextObject.SetActive(false);
    }

    private void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();

        movementX = movementVector.x;
        movementY = movementVector.y;
    }

    void SetCountText()
    {
        countText.text = "Count: " + score.ToString();
        if(count >= 20)
        {
            winTextObject.SetActive(true);
            //this stops the timer whenever all 20 pickups items have been picked up.
            timer.Stop = true;
        }
    }

    private void FixedUpdate()
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);

        rb.AddForce(movement * speed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUp")) 
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            score = score + 1;
            //tracker was for timer purposes I explain below
            tracker = tracker + 1;
            SetCountText();
        }
        //added seperate if-statement for second pickup items
        else if (other.gameObject.CompareTag("PickUp2"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            //these items add 2 points to the score
            score = score + 2;
            tracker = tracker + 1;
            SetCountText();
        }
        //added seperate if-statement for third pickups items
        else if (other.gameObject.CompareTag("PickUp3"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            //these items add 3 points to the score.
            score = score + 3;
            //tracker was added to try and adding a second to the timer whenever an item was picked up 
            //but I was not able to completely figure it out.
            tracker = tracker + 1;
            SetCountText();
        }
    }
}