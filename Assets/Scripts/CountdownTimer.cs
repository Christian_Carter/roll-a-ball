﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CountdownTimer : MonoBehaviour
{

   public float currentTime = 0f;
   float startingTime = 30f;

   public TextMeshProUGUI timerText;
   public bool Stop;
   public PlayerController addtimer;
   public PlayerController addtracker;

   //setting currentTime = startingTime or 30 seconds
   void Start()
   {
       currentTime = startingTime;
   }

   void Update()

   {
       //if the timer hasn't stopped, the timer will keep going
       if (Stop == false)
       {
           //makes the timer countdown every second from 30 seconds.
           currentTime -= 1 * Time.deltaTime;
           //writes the time as a string so it can displayed in the game.
           timerText.text = currentTime.ToString("0");
           //this keeps the timer from counting down past 0 seconds.
           if (currentTime <= 0)
           {
               currentTime = 0;
            }
          //  else if (addtracker.tracker >= 1)
          //  {
           //     currentTime += 1 * Time.deltaTime;
           //     timerText.text = currentTime.ToString("0");
          //  }
          //  else
           // {
           //     addtracker.tracker -= 1;
           // }
       }
   }
}